﻿using CSSD.Entities.SIM;
using CSSD.Helper;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;
using System.Linq.Dynamic;
using CSSD.Models.Transaksi;
using System.Security.Cryptography.X509Certificates;
using System.Web.ModelBinding;

namespace CSSD.Controllers.Transaksi
{
    public class LinenKotorPasienController : Controller
    {
        // GET: LinenKotorPasien
        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.CSSD_ListLinenKotorPasien.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "PeriodeStart" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.Tanggal >= d);
                        }
                        else if (x.Key == "PeriodeEnd" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.Tanggal <= d);
                        }
                        else if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y =>
                                y.No_Bukti.Contains(x.Value) ||
                                y.TypeTransaksi.Contains(x.Value) ||
                                y.Keterangan.Contains(x.Value) ||
                                y.NRM.Contains(x.Value) ||
                                y.NamaPasien.Contains(x.Value) ||
                                y.SectionName.Contains(x.Value) 
                            );

                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "DESC" : "ASC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        No_Bukti = x.No_Bukti,
                        Tanggal = x.Tanggal.ToString("dd/MM/yyyy"),
                        Jml_Item = x.Jml_Item,
                        Nilai_Transaksi = x.Nilai_Transaksi,
                        Total_Nilai = x.Total_Nilai,
                        Keterangan = x.Keterangan ?? "-",
                        TypeTransaksi = x.TypeTransaksi,
                        NRM = x.NRM,
                        NamaPasien = x.NamaPasien,
                        SectionName = x.SectionName,
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(string _process, LinenKotorPasienModel model)
        {
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        var userid = User.Identity.GetUserId();
                        string id = "";
                        if (_process == "CREATE")
                        {
                            if (model.Detail == null) model.Detail = new List<LinenKotorPasienModelDetailModel>();
                            if (model.Detail.Count == 0) throw new Exception("Detail tidak boleh kosong");
                            id = s.CSSD_InsertHeaderLinenKotorPasien(
                                model.Tanggal,
                                model.Waktu_Transaksi,
                                model.Tanggal_Selesai,
                                model.Waktu_Selesai,
                                model.Keterangan,
                                model.NoReg
                            ).FirstOrDefault();

                            foreach (var x in model.Detail)
                            {
                                var barang = s.CSSD_GetDataLaundryLinenKotor.FirstOrDefault(z => z.Kode == x.Kode_Item);
                                s.CSSD_InsertDetailLinenKotorPasien(
                                    id,
                                    x.Kode_Item,
                                    x.Qty,
                                    x.Diskon,
                                    barang == null? 0 : barang.H_Laundry,
                                    x.Keterangan,
                                    x.NOUrut
                                );
                            }
                            s.SaveChanges();
                        }

                        else if (_process == "DELETE")
                        {
                            id = model.No_Bukti;
                            s.CSSD_DeleteLinenKotorPasien(id, model.Alasan_Batal);
                            s.SaveChanges();
                        }

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"LinenKotorPasien-{_process}; id:{id};".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();

                        return JsonConvert.SerializeObject(new { IsSuccess = true, Id = id });
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
        }

        [HttpPost]
        public string Detail(string id)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.CSSD_GetHeaderLinenKotorPasien.FirstOrDefault(x => x.No_bukti == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    var d = s.CSSD_GetDetailLinenKotorPasien.Where(x => x.No_Bukti == m.No_bukti).ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            No_Bukti = m.No_bukti,
                            Tanggal = m.Tanggal.ToString("yyyy-MM-dd"),
                            Waktu_Transaksi = m.Waktu_Transaksi.Value.ToString("hh:mm"),
                            Tanggal_Selesai = m.Tanggal_Selesai.Value.ToString("yyyy-MM-dd"),
                            Waktu_Selesai = m.Waktu_Selesai.Value.ToString("hh:mm"),
                            Keterangan = m.Keterangan,
                            NoReg = m.NoReg,
                            NRM = m.NRM,
                            NamaPasien = m.NamaPasien,
                        },
                        Detail = d.ConvertAll(x => new
                        {
                            No_Bukti = x.No_Bukti,
                            Kode_Item = x.Kode,
                            Nama_Barang = x.Nama,
                            Qty = x.Qty,
                            Harga_Satuan = x.Harga_Satuan,
                            Diskon = x.Diskon,
                            SubTotal = x.SubTotal,
                            Keterangan = x.Keterangan,
                            NOUrut = x.NOUrut
                        })
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== L O O K U P

        [HttpPost]
        public string ListLookupPasien(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.CSSD_GetDataLaundryLinenKotor.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y =>
                                y.Kode.Contains(x.Value) ||
                                y.Nama_Barang.Contains(x.Value));
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        Kode_Item = x.Kode,
                        Nama_Barang = x.Nama_Barang,
                        H_Laundry = x.H_Laundry,
                       
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== L O O K U P - NRM

        [HttpPost]
        public string ListLookupNRM(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.CSSD_GetDataPasienLinenKotor.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y =>
                                y.NoReg.Contains(x.Value) ||
                                y.NRM.Contains(x.Value) ||
                                y.NamaPasien.Contains(x.Value));
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        NoReg = x.NoReg,
                        NRM = x.NRM,
                        NamaPasien = x.NamaPasien,
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion
    }
}


//else if (_process == "EDIT")
//{
//    if (model.Detail == null) model.Detail = new List<OtherPaketObatDetailModel>();

//    s.ADM_UpdatePaketObat(
//        model.Id,
//        model.Nama,
//        model.Section,
//        DateTime.Today,
//        model.Dokter,
//        model.ObatPuyer,
//        model.Keterangan
//    );

//    var d = s.ADM_GetDetailPaketObat.Where(x => x.KodePaket == model.Id).ToList();
//    foreach (var x in d)
//    {
//        var _d = model.Detail.FirstOrDefault(y => y.Id == x.Barang_ID);
//        //delete
//        if (_d == null) s.ADM_DeleteDetailPaketObat(model.Id, x.Barang_ID);
//    }
//    foreach (var x in model.Detail)
//    {
//        var _d = d.FirstOrDefault(y => y.Barang_ID == x.Id);
//        if (_d == null)
//        {
//            // new
//            s.ADM_InsertDetailPaketObat(
//                model.Id,
//                x.Id,
//                x.Qty,
//                x.AturanPakai
//            );
//        }
//        else
//        {
//            // edit
//            s.ADM_UpdateDetailPaketObat(
//                model.Id,
//                x.Id,
//                x.Qty,
//                x.AturanPakai
//            );
//        }

//    }
//    s.SaveChanges();
//}