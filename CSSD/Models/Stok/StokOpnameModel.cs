﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CSSD.Models.Stok
{
    public class StokOpnameViewModel
    {
        public string NoBukti { get; set; }
        public DateTime Tanggal { get; set; }
        public int Lokasi { get; set; }
        public string Jenis { get; set; }
        public List<StokOpnameDetailViewModel> Detail { get; set; }
    }

    public class StokOpnameDetailViewModel
    {
        public int Id { get; set; }
        public int Qty { get; set; }
        public string Keterangan { get; set; }
    }
   
}