﻿using CSSD.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CSSD.Models
{
    public class ReportViewModel
    {
        public string Name { get; set; }
        public string Category { get; set; }
        public List<HReportModel> Reports { get; set; }
    }
}