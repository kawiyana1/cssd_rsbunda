﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CSSD.Models.Transaksi
{
    public class PenerimaanLinenDariVerndorModel
    {
        public decimal No_Penerimaan { get; set; }
        public DateTime Tanggal { get; set; }
        public string Keterangan { get; set; }
        public string No_DO { get; set; }
        public string Kode_Supplier { get; set; }
        public short Lokasi_ID { get; set; }
        public DateTime? Tgl_JatuhTempo { get; set; }
        public int No_TT { get; set; }
        public decimal Total_Nilai { get; set; }
        public decimal Potongan { get; set; }

        public List<PenerimaanLinenDariVerndordetailModel> Detail { get; set; }
    }

    public class PenerimaanLinenDariVerndordetailModel
    {
        public string No_Penerimaan { get; set; }
        public Nullable<decimal> Penerimaan_ID { get; set; }
        public string Kode_Barang { get; set; }
        public string Nama_Barang { get; set; }
        public string Satuan_Stok { get; set; }
        public double Qty_TT { get; set; }
        public double Tlh_Diterima { get; set; }
        public double Qty_Penerimaan { get; set; }
        public double Qty_Retur { get; set; }
        public decimal Harga_Beli { get; set; }
        public double Diskon_1 { get; set; }
        public decimal Diskon_Rp { get; set; }
        public Nullable<int> Barang_ID { get; set; }

    }
}