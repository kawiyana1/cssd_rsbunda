﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CSSD.Startup))]
namespace CSSD
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
