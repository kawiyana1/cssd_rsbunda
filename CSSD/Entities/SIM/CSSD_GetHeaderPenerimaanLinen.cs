//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CSSD.Entities.SIM
{
    using System;
    using System.Collections.Generic;
    
    public partial class CSSD_GetHeaderPenerimaanLinen
    {
        public System.DateTime Tgl_Penerimaan { get; set; }
        public string No_Penerimaan { get; set; }
        public string Kode_Supplier { get; set; }
        public string Nama_Supplier { get; set; }
        public string No_Order { get; set; }
        public Nullable<System.DateTime> Tgl_JatuhTempo { get; set; }
        public string Nama_Lokasi { get; set; }
        public string Remark { get; set; }
        public string No_DO { get; set; }
        public decimal Total_Nilai { get; set; }
        public Nullable<decimal> TotalDiskon { get; set; }
        public decimal Penerimaan_ID { get; set; }
    }
}
